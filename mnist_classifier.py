""" Tensorflow Example - Softmax """
import numpy as np
import tensorflow as tf
# import os
# os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

def fc_layer_init_xavier(in_node, dims, act_fn=tf.identity, name = 'fc'):
#   Fully connected feed-forward layer with randomly initialized network
#   :param in_node : input node
#   :param dims    : [input_dims, output_dims]
#   :param act_fn  : activation function
#   :param name    : name of the scope
#   :return        : activated neural nodes
    stddev = np.reciprocal(np.sqrt((dims[0] + 1) * dims[1]))
    with tf.name_scope(name):
        w = tf.Variable(tf.random_normal([dims[0], dims[1]], stddev=stddev))
        b = tf.Variable(tf.random_normal([dims[1]], stddev=stddev))

        return act_fn(tf.add(tf.matmul(in_node, w), b))

def evaluate_accuracy(logits, labels, name='accuracy'):
    with tf.name_scope(name):
        prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(labels, 1))

        return (1/2) * tf.reduce_mean(tf.cast(prediction, tf.float32))

# def evaluate_mse(logits, labels, name='mse'):
#     with tf.name_scope(name):
#         diff = tf.subract(logits, labels)
#         mse = tf.square(diff)
#
#         return tf.reduce_mean(tf.cast(mse, tf.float32))

def binary_read(filename, hsize=0, ndim=-1, htype = 'f', dtype='f'):
    with open(filename, 'rb') as fid:
        header = np.fromfile(fid, dtype=np.dtype(htype), count=hsize)
        data = []
        while True:
            # tmp = fid.read(ndim)
            tmp = np.fromfile(fid, dtype=np.dtype(dtype), count=ndim)
            # print(tmp)
            if len(tmp) == 0:
                break
            # data.append(np.fromfile(fid, dtype=np.dtype(dtype), count=ndim))
            data.append(tmp)
            # data = np.append(data, tmp)
        return header, data

def num2class(data, dim):
#   convert number from mnist database to class for softmax (hotkey)
#   :data   : the number of output
#   :dim    : the dimension of output
#   :return : hotkey
    class_data = []
    for iSample in range(len(data)):

        index = data[iSample]
        a = np.array([0] * (dim - index[0] - 1))
        b = np.array([0] * (index[0]))

        temp = np.concatenate((a, [1], b))

        if index == dim-1:
          temp = np.concatenate(([0] * (dim-1), [1]))
        if index == 0:
          temp = np.concatenate(([1], [0] * (dim-1)))

        class_data.append(temp)
    return class_data

# File Load
filepath = '/home/soowhan/DSP/Database/MNIST/train-images.idx3-ubyte'
train_x_header, train_x = binary_read(filepath, hsize=4, htype=np.int32, ndim=28*28, dtype=np.ubyte)

filepath = '/home/soowhan/DSP/Database/MNIST/train-labels.idx1-ubyte'
train_y_header, train_y = binary_read(filepath, hsize=2, htype=np.int32, ndim=1, dtype=np.ubyte)
train_y = num2class(train_y, 10)

filepath = '/home/soowhan/DSP/Database/MNIST/t10k-images.idx3-ubyte'
test_x_header, test_x = binary_read(filepath, hsize=4, htype=np.int32, ndim=28*28, dtype=np.ubyte)

filepath = '/home/soowhan/DSP/Database/MNIST/t10k-labels.idx1-ubyte'
test_y_header, test_y = binary_read(filepath, hsize=2, htype=np.int32, ndim=1, dtype=np.ubyte)
test_y = num2class(test_y, 10)

dim_input = np.shape(train_x)[1]
dim_output = np.shape(train_y)[1]


print('# of training sample: %d' % np.shape(train_x)[0])
print('# of test sample    : %d' % np.shape(test_x)[0])
print('Dimension of input  : %d' % dim_input)
print('Dimension of output : %d' % dim_output)

## Neural Network
# Create the model
dim_hidden = [1024, 1024, 1024]

x = tf.placeholder(tf.float32, [None, dim_input])                       # Declare the input layer
h1 = fc_layer_init_xavier(x, [dim_input, dim_hidden[0]], tf.tanh)       # Declare the first hidden layer
h2 = fc_layer_init_xavier(h1, [dim_hidden[0], dim_hidden[1]], tf.tanh)  # Declare the second hidden layer
h3 = fc_layer_init_xavier(h2, [dim_hidden[1], dim_hidden[2]], tf.tanh)  # Declare the second hidden layer
y = fc_layer_init_xavier(h3, [dim_hidden[2], dim_output])               # Declare the output layer
target = tf.placeholder(tf.float32, [None, dim_output])                 # Declare the place for target data

# Define loss and optimizer
CE = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=target, logits=y)) # Criterion: Cross-entropy
MSE = tf.reduce_mean(tf.square(tf.subtract(target, y)))                               # Criterion: MSE
opt_op = tf.train.GradientDescentOptimizer(0.01).minimize(CE)                         # Optimization module
                                                                                      # Gradient Descent rule
sess = tf.InteractiveSession()           # Session loaded
tf.global_variables_initializer().run()  # Initializer


# Train
nEpoch = 1000
nSample = len(train_x)

batchsize = 100

nBatch = int(nSample / batchsize)

acc_train = []
acc_test = []
for iEpoch in range(nEpoch):


  # sess.run(opt_op, feed_dict={x: train_x, target: train_y})  # do optimization process
  for iteration in range(nBatch):
    iBgn = iteration * batchsize
    index = np.arange(batchsize) + iBgn
    batch_x = train_x[iBgn:batchsize+iBgn]
    batch_y = train_y[iBgn:batchsize+iBgn]

    sess.run(opt_op, feed_dict={x: batch_x, target: batch_y})  # do optimization process



  #   accuracy = evaluate_accuracy(y)
  accuracy = evaluate_accuracy(y, target)  # accuracy module

  train_accuracy = accuracy.eval(feed_dict={x: train_x, target: train_y})
  test_accuracy = accuracy.eval(feed_dict={x: test_x, target: test_y})
  acc_train.append(train_accuracy)  # check training accuracy
  acc_test.append(test_accuracy)  # check test accuracy

  print('Epoch #%d : %.4f / %.4f' % (iEpoch+1, train_accuracy, test_accuracy))




print(acc_train)
print(acc_test)

